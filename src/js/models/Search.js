import axios from "axios";
import { appID, key } from "../config";

export default class Search {
  constructor(query) {
    this.query = query;
  }

  async getResults() {
    try {
      const res = await axios(
        // `https://www.food2fork.com/api/search?key=${key}&q=${this.query}`
        `https://api.edamam.com/search?q=${
          this.query
        }&app_id=${appID}&app_key=${key}&from=0&to=30`
      );

      this.result = res.data.hits;
      // console.log("​getResults -> this.result", this.result);
    } catch (error) {
      alert(error);
    }
  }
}
